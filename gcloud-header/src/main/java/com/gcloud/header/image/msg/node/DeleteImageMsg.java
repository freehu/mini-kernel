package com.gcloud.header.image.msg.node;

import com.gcloud.header.NodeMessage;

public class DeleteImageMsg extends NodeMessage {
	private String imageId;

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
}
