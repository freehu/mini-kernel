
package com.gcloud.header.storage.msg.api.pool;

import java.util.List;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.storage.model.DiskCategoryModel;

public class ApiDescribeDiskCategoriesReplyMsg extends ApiReplyMessage {

    private static final long serialVersionUID = 1L;

    private List<DiskCategoryModel> models;

    public List<DiskCategoryModel> getModels() {
        return models;
    }

    public void setModels(List<DiskCategoryModel> models) {
        this.models = models;
    }

}
