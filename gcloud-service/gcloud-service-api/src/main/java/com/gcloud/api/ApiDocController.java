package com.gcloud.api;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.pegdown.PegDownProcessor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.core.util.ApiDocUtil;

@RestController
public class ApiDocController {
	@GetMapping("/apidoc")
	public String apiDoc(HttpServletResponse response) throws IOException, ClassNotFoundException{
		/*response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		 PegDownProcessor pdp = new PegDownProcessor(Integer.MAX_VALUE);
	     String html = pdp.markdownToHtml("[toc] \n\n# Controller \n## dd \n |sss|ddf|dd|");
		response.getWriter().print(html);
		*/
		//return null;
		//return "[[toc]]# Controller 0## VM 01### Create 01";
	    PegDownProcessor pdp = new PegDownProcessor(Integer.MAX_VALUE);
		return pdp.markdownToHtml(ApiDocUtil.getInstance().getDoc());
	    //return ApiDocUtil.getInstance().getDoc();
	}
}
