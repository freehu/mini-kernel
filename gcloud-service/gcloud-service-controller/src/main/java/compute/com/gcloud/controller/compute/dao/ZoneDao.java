
package com.gcloud.controller.compute.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;

@Jdbc("controllerJdbcTemplate")
@Repository
public class ZoneDao extends JdbcBaseDaoImpl<AvailableZoneEntity, String> {

}
