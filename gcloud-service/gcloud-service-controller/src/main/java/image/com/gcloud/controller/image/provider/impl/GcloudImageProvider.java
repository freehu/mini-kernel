
package com.gcloud.controller.image.provider.impl;

import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.provider.IImageProvider;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.image.msg.api.GenDownloadVo;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class GcloudImageProvider implements IImageProvider {

    @Override
    public ResourceType resourceType() {
        return ResourceType.IMAGE;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.GCLOUD;
    }

    @Override
    public String createImage(CreateImageParams params, CurrentUser currentUser) throws GCloudException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void updateImage(String imageId, String imageProviderRefId, String imageName) throws GCloudException {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteImage(String imageId, String imageProviderRefId) throws GCloudException {
        // TODO Auto-generated method stub

    }

    public List<Image> listImage(Map<String, String> filters) throws GCloudException {
        throw new GCloudException("no need to implement.");
    }

    @Override
    public GenDownloadVo genDownload(String imageId, String providerRefId) {
        // TODO Auto-generated method stub
        return null;
    }
}
