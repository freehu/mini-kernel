package com.gcloud.controller.network.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.entity.Router;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.CreateVSwitchParams;
import com.gcloud.controller.network.model.DescribeVSwitchesParams;
import com.gcloud.controller.network.provider.ISubnetProvider;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.model.VSwitchSetType;
import com.gcloud.header.network.msg.api.CreateVSwitchMsg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class SubnetServiceImpl implements ISubnetService {
    @Autowired
    private SubnetDao subnetDao;
    @Autowired
    private NetworkDao networkDao;

    @Override
    public PageResult<VSwitchSetType> describeVSwitches(DescribeVSwitchesParams params, CurrentUser currentUser) {
        return subnetDao.describeVSwitches(params, VSwitchSetType.class, currentUser);
    }

    @Override
    public String createSubnet(CreateVSwitchParams params, CurrentUser currentUser) {
        Network network = networkDao.getById(params.getVpcId());
        if (network == null) {
            throw new GCloudException("0030402::找不到网络");
        }
        String subnetId = StringUtils.genUuid();
        this.getProviderOrDefault().createSubnet(network, subnetId, params, currentUser);
        return subnetId;
    }

    @Override
    public void deleteSubnet(String subnetId) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030402::找不到交换机");
        }
        subnetDao.deleteById(subnetId);
        this.checkAndGetProvider(subnet.getProvider()).deleteSubnet(subnet.getProviderRefId());
        
    }

    @Override
    public void modifyAttribute(String subnetId, String subnetName) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030203::找不到交换机");
        }
        List<String> updateField = new ArrayList<String>();
        updateField.add(subnet.updateName(subnetName));
        updateField.add(subnet.updateUpdatedAt(new Date()));
        subnetDao.update(subnet, updateField);
        this.checkAndGetProvider(subnet.getProvider()).modifyAttribute(subnet.getProviderRefId(), subnetName);
        
    }
    
    @Override
    public String createVSwitch(CreateVSwitchMsg params, CurrentUser currentUser) {
//        Network network = networkDao.getById(params.getVpcId());
        if (params.getNetworkId() == null) {
            params.setNetworkId("");
        }
        if (params.getVpcId() == null) {
        	params.setVpcId("");
        }
        return  this.getProviderOrDefault().createVSwitch(params, currentUser);
    }

    @Override
    public void deleteVSwitch(String subnetId) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030402::找不到交换机");
        }
        this.checkAndGetProvider(subnet.getProvider()).deleteVSwitch(subnet.getProviderRefId());
        
    }

    private ISubnetProvider getProviderOrDefault() {
        ISubnetProvider provider = ResourceProviders.getDefault(ResourceType.SUBNET);
        return provider;
    }

    private ISubnetProvider checkAndGetProvider(Integer providerType) {
        ISubnetProvider provider = ResourceProviders.checkAndGet(ResourceType.SUBNET, providerType);
        return provider;
    }
}
