
package com.gcloud.controller.storage.handler.api.pool;

import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiCreateDiskCategoryMsg;
import com.gcloud.header.storage.msg.api.pool.ApiCreateDiskCategoryReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
import org.springframework.beans.factory.annotation.Autowired;

@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.CREATE_DISK_CATEGORRY)
public class ApiCreateDiskCategoryHandler extends MessageHandler<ApiCreateDiskCategoryMsg, ApiCreateDiskCategoryReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiCreateDiskCategoryReplyMsg handle(ApiCreateDiskCategoryMsg msg) throws GCloudException {
        ApiCreateDiskCategoryReplyMsg reply = new ApiCreateDiskCategoryReplyMsg();
        reply.setTypeId(this.poolService.createDiskCategory(msg.getZoneId(), msg.getCode(), msg.getName(), msg.getMinSize(), msg.getMaxSize()));
        return reply;
    }

}
