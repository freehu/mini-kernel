
package com.gcloud.controller.storage.handler.api.pool;

import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiDescribeDiskCategoriesMsg;
import com.gcloud.header.storage.msg.api.pool.ApiDescribeDiskCategoriesReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
import org.springframework.beans.factory.annotation.Autowired;

@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.DESCRIBE_DISK_CATEGORIES)
public class ApiDescribeDiskCategoriesHandler extends MessageHandler<ApiDescribeDiskCategoriesMsg, ApiDescribeDiskCategoriesReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiDescribeDiskCategoriesReplyMsg handle(ApiDescribeDiskCategoriesMsg msg) throws GCloudException {
        ApiDescribeDiskCategoriesReplyMsg reply = new ApiDescribeDiskCategoriesReplyMsg();
        reply.setModels(this.poolService.describeDiskCategories(msg.getZoneId()));
        return reply;
    }

}
