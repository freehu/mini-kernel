
package com.gcloud.storage.driver;

import java.util.List;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.storage.enums.StoragePoolDriver;

public interface IStorageDriver {

    StoragePoolDriver driver();

    void createDisk(String poolName, String volumeId, Integer size, String imageId) throws GCloudException;

    void deleteDisk(String poolName, String volumeId) throws GCloudException;

    void resizeDisk(String poolName, String volumeId, Integer oldSize, Integer newSize) throws GCloudException;

    void createSnapshot(String poolName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException;

    void deleteSnapshot(String poolName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException;

    List<String> resetSnapshot(String poolName, String volumeRefId, String snapshotId, String snapshotRefId, Integer size) throws GCloudException;

}
