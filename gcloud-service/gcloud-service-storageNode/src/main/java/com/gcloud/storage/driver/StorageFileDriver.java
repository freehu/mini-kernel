
package com.gcloud.storage.driver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.enums.FileFormat;
import com.gcloud.header.compute.enums.Unit;
import com.gcloud.header.image.msg.api.ApiGenDownloadMsg;
import com.gcloud.header.image.msg.api.ApiGenDownloadReplyMsg;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.compute.uitls.LogUtil;
import com.gcloud.storage.StorageNodeProp;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StorageFileDriver implements IStorageDriver {

    private static final String EMPTY_IMAGE_NAME = "empty.qcow2";

    @Autowired
    private StorageNodeProp props;

    @Autowired
    private MessageBus bus;

    @Override
    public StoragePoolDriver driver() {
        return StoragePoolDriver.FILE;
    }

    public void createDisk(String poolName, String volumeId, Integer size, String imageId) throws GCloudException {
        String volumePath = this.genDiskPath(poolName, volumeId);
        String imagePath = imageId == null ? this.getEmptyImage(poolName) : this.checkAndDownloadImage(imageId);
        DiskQemuImgUtil.create(imagePath, volumePath, size, Unit.G);
    }

    private String getEmptyImage(String poolName) {
        String path = this.genDiskPath(poolName, EMPTY_IMAGE_NAME);
        File file = new File(path);
        if (!file.exists()) {
            synchronized (StorageFileDriver.class) {
                file = new File(path);
                if (!file.exists()) {
                    DiskQemuImgUtil.create(1, "G", path, FileFormat.QCOW2.getValue());
                }
            }
        }
        return path;
    }

    private String genDiskPath(String poolName, String volumeId) {
        return poolName + volumeId;
    }

    private String checkAndGet(String poolName, String volumeId) {
        String path = this.genDiskPath(poolName, volumeId);
        this.checkAndGetFile(path);
        return path;
    }

    private File checkAndGetFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_VOLUME);
        }
        return file;
    }

    synchronized private String checkAndDownloadImage(String imageId) {
        String imagePath = this.props.getCachedImagesPath();
        if (!imagePath.endsWith(File.separator)) {
            imagePath += File.separator;
        }
        File imageDir = new File(imagePath);
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
        File imageFile = new File(imagePath + imageId);
        if (!imageFile.exists()) {
            String tmp = imagePath + imageId + ".tmp";
            log.info("image not exist, downloading {}", tmp);
            ApiGenDownloadMsg msg = new ApiGenDownloadMsg();
            msg.setServiceId(MessageUtil.controllerServiceId());
            msg.setImageId(imageId);
            ApiGenDownloadReplyMsg reply = this.bus.call(msg, ApiGenDownloadReplyMsg.class);
            if (!reply.getSuccess()) {
                throw new GCloudException(reply.getErrorMsg());
            }
            String[] cmd = new String[] {"glance", "--os-image-url", reply.getDownloadInfo().getServiceUrl(), "--os-auth-token", reply.getDownloadInfo().getTokenId(),
                    "image-download", reply.getDownloadInfo().getImageRefId(), "--file", tmp};
            int res = SystemUtil.runAndGetCode(cmd);
            LogUtil.handleLog(cmd, res, "::下载镜像失败");
            SystemUtil.runAndGetCode(new String[] {"mv", tmp, imagePath + imageId});
        }
        return imagePath + imageId;
    }

    @Override
    public void deleteDisk(String poolName, String volumeId) throws GCloudException {
        String diskPath = this.genDiskPath(poolName, volumeId);
        File diskFile = new File(diskPath);
        if (diskFile.exists()) {
            if (!diskFile.delete()) {
                throw new GCloudException(StorageErrorCodes.FAILED_TO_DELETE_VOLUME);
            }
        }
    }

    @Override
    public void resizeDisk(String poolName, String volumeId, Integer oldSize, Integer newSize) throws GCloudException {
        String diskPath = this.genDiskPath(poolName, volumeId);
        this.checkAndGetFile(diskPath);
        DiskQemuImgUtil.resize(diskPath, newSize - oldSize);
    }

    @Override
    public void createSnapshot(String poolName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException {
        String diskPath = this.checkAndGet(poolName, volumeRefId);
        String snapshotPath = this.genDiskPath(poolName, snapshotRefId);
        SystemUtil.runAndGetCode(new String[] {"mv", diskPath, snapshotPath});
        DiskQemuImgUtil.create(snapshotPath, diskPath);
    }

    @Override
    public void deleteSnapshot(String poolName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException {
        String diskPath = this.checkAndGet(poolName, volumeRefId);
        String snapshotPath = this.checkAndGet(poolName, snapshotRefId);
        File snapshotFile = new File(snapshotPath);
        if (snapshotFile.exists()) {
            String[] backingChain = this.backingChain(diskPath);
            String snapshotParentPath = null;
            String snapshotChildPath = null;
            for (int i = 0; i < backingChain.length; i++) {
                if (backingChain[i].equals(snapshotPath)) {
                    snapshotParentPath = backingChain[i + 1];
                    snapshotChildPath = backingChain[i - 1];
                    break;
                }
            }
            DiskQemuImgUtil.rebase(snapshotChildPath, snapshotParentPath);
            snapshotFile.delete();
        }
    }

    @Override
    public List<String> resetSnapshot(String poolName, String volumeRefId, String snapshotId, String snapshotRefId, Integer size) throws GCloudException {
        String diskPath = this.checkAndGet(poolName, volumeRefId);
        String[] backingChain = this.backingChain(diskPath);
        String snapshotPath = this.checkAndGet(poolName, snapshotRefId);
        DiskQemuImgUtil.create(snapshotPath, diskPath, size, Unit.G);
        List<String> snapshotsToDelete = new ArrayList<>();
        for (String path : backingChain) {
            if (path.equals(diskPath)) {
                continue;
            }
            if (path.equals(snapshotPath)) {
                break;
            }
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
            snapshotsToDelete.add(path.substring(path.lastIndexOf(File.separator) + 1));
        }
        return snapshotsToDelete;
    }

    // system: volume - snap3 - snap2 - snap1 - image
    // data:   volume - snap3 - snap2 - snap1
    private String[] backingChain(String path) {
        List<String> chain = new ArrayList<>();
        String res = SystemUtil.run(new String[] {"qemu-img", "info", path, "--backing-chain"});
        for (String line : res.split("\n")) {
            if (line.startsWith("image: ")) {
                chain.add(line.substring("image: ".length()));
            }
        }
        return chain.toArray(new String[] {});
    }

}
