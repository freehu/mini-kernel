
package com.gcloud.storage.service.impl;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.storage.StorageNodeProp;
import com.gcloud.storage.driver.StorageDrivers;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NodeSnapshotServiceImpl {

    @Autowired
    private StorageNodeProp props;

    @Autowired
    private MessageBus bus;

    @Autowired
    private StorageDrivers drivers;

    public void createSnapshot(String storageType, String poolName, String driverName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException {
        drivers.get(driverName).createSnapshot(poolName, volumeRefId, snapshotId, snapshotRefId);
    }

    public void deleteSnapshot(String storageType, String poolName, String driverName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException {
        drivers.get(driverName).deleteSnapshot(poolName, volumeRefId, snapshotId, snapshotRefId);
    }

    public List<String> resetSnapshot(String storageType, String poolName, String driverName, String volumeRefId, String snapshotId, String snapshotRefId, Integer size)
            throws GCloudException {
        return drivers.get(driverName).resetSnapshot(poolName, volumeRefId, snapshotId, snapshotRefId, size);
    }

}
