
package com.gcloud.storage.service.impl;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.storage.driver.StorageDrivers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NodeVolumeServiceImpl {

    @Autowired
    private StorageDrivers drivers;

    public void createDisk(String storageType, String poolName, String driverName, String volumeId, Integer size, String imageId) throws GCloudException {
        drivers.get(driverName).createDisk(poolName, volumeId, size, imageId);
    }

    public void deleteDisk(String storageType, String poolName, String driverName, String volumeId) throws GCloudException {
        drivers.get(driverName).deleteDisk(poolName, volumeId);
    }

    public void resizeDisk(String storageType, String poolName, String driverName, String volumeId, Integer oldSize, Integer newSize) throws GCloudException {
        drivers.get(driverName).resizeDisk(poolName, volumeId, oldSize, newSize);
    }

}
